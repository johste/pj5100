﻿using UnityEngine;
using System.Collections;

public class TranquilizeGuard : MonoBehaviour {

    public GameObject tranq;
    public GameObject sleepEfx;
    public AudioClip sleepSFX;
    public TextMesh stunnedText;

    private GuardAI gAI;
    private GuardVision gV;
    private GameObject player;

    //It's a word i promise
    private int stepsSinceTranquilization;
    private int sleepLength = 4;
    private int ticksAsleep;
    private bool isTranquilized;
    private GameObject lastTranq;

    private void Awake() {

        gAI = GetComponent<GuardAI>();
        gV = GetComponent<GuardVision>();
        sleepEfx.SetActive(false);
    }

    private void Start()
    {
        player = Service.Connect("Player");
    }


    private void OnEnable()
    {
        EasyTouch.On_TouchUp += TranquilizeBear;
        GameMaster.OnLateTimeTick += UnTranquilizeBear;
    }

    private void OnDisable()
    {
        EasyTouch.On_TouchUp -= TranquilizeBear;
        GameMaster.OnLateTimeTick -= UnTranquilizeBear;
    }

    private void OnDestroy()
    {
        EasyTouch.On_TouchUp -= TranquilizeBear;
        GameMaster.OnLateTimeTick -= UnTranquilizeBear;
    }

    private void TranquilizeBear(Gesture gesture) {

        if (GameMaster.tranqSelected && GameMaster.tranquilizersLeft > 0) {
            if (gesture.pickedObject == this.gameObject && !isTranquilized) {

                /*
                RaycastHit hit;
                Vector3 rayDirection = player.transform.position - transform.position;
                if (Physics.Raycast(transform.position, rayDirection, out hit))
                {
                    if (hit.transform.gameObject == player) return;
                }
                */

                //Debug.Log("BEAR HIT WITH TRANQUILIZER");
                if(sleepSFX)Service.sfxManager.PlaySFX(sleepSFX);

                SpawnTranq();

                sleepEfx.SetActive(true);
                gAI.CanMove(false);
                gV.CanSee(false);
                GameMaster.current.EnableTranq(false);
                GameMaster.tranquilizersLeft--;
                isTranquilized = true;
                stunnedText.text = "" + (sleepLength - ticksAsleep -1);
                stunnedText.gameObject.SetActive(true);
                
            }
        }
    }

    private void SpawnTranq()
    {
        lastTranq = Instantiate(tranq, player.transform.position, Quaternion.identity) as GameObject;
    }

    void Update()
    {
        if(lastTranq != null)
        {
            lastTranq.transform.position = Vector3.MoveTowards(lastTranq.transform.position, transform.position, Time.deltaTime * 20f);
            lastTranq.transform.LookAt(transform.position);
            if(Vector3.Distance(transform.position, lastTranq.transform.position) < 0.15f)
            {
                Destroy(lastTranq);
                lastTranq = null;
            }
        }
    }

    private void UnTranquilizeBear() {

        if (isTranquilized) {
            //Debug.Log("BEAR IS TRANQUILIZED, COUNT UP " + ticksAsleep);
            ticksAsleep++;
            stunnedText.text = "" + (sleepLength - ticksAsleep -1);
            if (ticksAsleep >= sleepLength) {
                //Debug.Log("BEAR IS NO LONGER ASLEEP");
                stunnedText.gameObject.SetActive(false);
                ticksAsleep = 0;
                isTranquilized = false;
                sleepEfx.SetActive(false);
                gAI.CanMove(true);
                gV.CanSee(true);
            }
        }
    }
}
