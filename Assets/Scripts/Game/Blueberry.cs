﻿using UnityEngine;
using System.Collections;

public class Blueberry : Pickup {

    public int amount = 1;

    override protected void PickUp()
    {
        GameController.current.AddBlueBerry(amount);
    }
}
