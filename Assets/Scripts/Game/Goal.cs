﻿using UnityEngine;
using System.Collections;

public class Goal : Pickup {

    public AudioClip singletonSfx;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace)) PickUp();
    }

    override protected void PickUp()
    {
        Service.Connect("Canvas").GetComponent<FadeOutWithAlpha>().OnClick();
        Service.blinds.CloseBlinds();

        if (Service.save.GetValue<int>("LevelsUnlocked") < (Application.loadedLevel - 4))
        {
            Service.save.SetValue("LevelsUnlocked", Service.save.GetValue<int>("LevelsUnlocked") + 1);
            Service.save.Save();
        }

        //Debug.Log(Application.loadedLevel - 4);
        if (Application.loadedLevel - 4 < 8)
        {
            Service.save.SetValue("World", 0);
            Service.Connect("GameMaster").GetComponent<GameMaster>().GoToLevel(1);
        }

        if (Application.loadedLevel - 4 >= 8 && Application.loadedLevel - 4 < 16)
        {
            Service.save.SetValue("World", 1);
            Service.Connect("GameMaster").GetComponent<GameMaster>().GoToLevel(2);
        }

        if (Application.loadedLevel - 4 >= 16 && Application.loadedLevel - 4 < 24)
        {
            Service.save.SetValue("World", 2);
            Service.Connect("GameMaster").GetComponent<GameMaster>().GoToLevel(3);
        }
        /*
        if (Application.loadedLevel - 4 >= 24 && Application.loadedLevel - 4 < 32)
        {
            Service.save.SetValue("World", 3);
            Service.Connect("GameMaster").GetComponent<GameMaster>().GoToLevel(4);
        }
        */

        if(Application.loadedLevel == 28)
        {
            Service.save.SetValue("World", 2);
            Service.Connect("GameMaster").GetComponent<GameMaster>().GoToLevel(0);
        }

        Service.save.Save();
        if (MusicManager.current != null) MusicManager.current.PlaySingletonSFX(singletonSfx, 1, 1);
	}
}
