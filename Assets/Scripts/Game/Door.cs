﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

    private bool isLocked = true;
    private bool isOpen;
    public Transform door;
    public Transform keyLock;
    public SpriteRenderer notification;
    public Sprite[] sprites;
    private float defaultEulerAnglesY;

    public enum KeyColor { None ,Red, Blue, Green };
    public KeyColor requiredKey;

    private float angle;
    private float point;
    private GameObject player;
    private BoxCollider boxCollider;
    private bool hasRequiredKey;

	void Start ()
    {
        boxCollider = GetComponent<BoxCollider>();
        player = Service.Connect("Player");
        defaultEulerAnglesY = door.eulerAngles.y;
        hasRequiredKey = false;
        CheckStatus();
    }
	
	void Update ()
    {
        keyLock.gameObject.SetActive(isLocked);

        if (!isLocked)
        {
            if (isOpen)
            {
                if (point < 1f) point += Time.deltaTime;
                angle = Mathf.Lerp(angle, 90f, point);
                boxCollider.enabled = false;
            }
            else
            {
                if (point > 0f) point -= Time.deltaTime;
                angle = Mathf.Lerp(angle, 0f, 1 - point);
                boxCollider.enabled = true;
            }
            
            door.eulerAngles = new Vector3(door.eulerAngles.x, defaultEulerAnglesY + angle, door.eulerAngles.z);
        }
        if (Vector3.Distance(player.transform.position, transform.position) < 2.5f) isOpen = true;
        else isOpen = false;
	}

    private void CheckStatus()
    {
        switch (requiredKey)
        {
            case KeyColor.None:
                isLocked = false;
                break;
            case KeyColor.Red:
                notification.sprite = sprites[0];
                isLocked = !Service.Keychain.hasRedKey;
                break;
            case KeyColor.Blue:
                notification.sprite = sprites[1];
                isLocked = !Service.Keychain.hasBlueKey;
                break;
            case KeyColor.Green:
                notification.sprite = sprites[2];
                isLocked = !Service.Keychain.hasGreenKey;
                break;
        }
        keyLock.gameObject.SetActive(isLocked);
        notification.enabled = isLocked;
    }

    private void OnEnable()
    {
        GameMaster.OnTimeTick += CheckStatus;
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    private void OnDestroy()
    {
        Unsubscribe();
    }

    private void Unsubscribe()
    {
        GameMaster.OnTimeTick -= CheckStatus;
    }
}
