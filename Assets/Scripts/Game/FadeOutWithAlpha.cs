﻿using UnityEngine;
using System.Collections;

public class FadeOutWithAlpha : MonoBehaviour {

    private CanvasGroup cG;
    private bool fadeOut;

	void Start ()
    {
        cG = GetComponent<CanvasGroup>();
	}

	void Update ()
    {
        if (fadeOut) cG.alpha -= Time.deltaTime *2;
	}

    public void OnClick()
    {
        fadeOut = true;
    }
}
