﻿using UnityEngine;
using System.Collections;

public class Key : Pickup {

    public enum KeyColor { Red, Blue, Green };
    public KeyColor keyColor;
    public Color[] colors;


    private void Awake()
    {
        Material m = GetComponentInChildren<Renderer>().material;
        //if (m == null) m = GetComponentInChildren<Renderer>().material;
        switch (keyColor)
        {
            case KeyColor.Red:
                m.color = colors[0];
                break;
            case KeyColor.Blue:
                m.color = colors[1];
                break;
            case KeyColor.Green:
                m.color = colors[2];
                break;
        }
    }

    protected override void PickUp ()
    {
        switch (keyColor)
        {
            case KeyColor.Red:
                Service.Keychain.hasRedKey = true;
                break;
            case KeyColor.Blue:
                Service.Keychain.hasBlueKey = true;
                break;
            case KeyColor.Green:
                Service.Keychain.hasGreenKey = true;
                break;
        }
	}
}
