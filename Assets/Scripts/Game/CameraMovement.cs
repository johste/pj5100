﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

    public Transform target;
    public Vector3 offset;

	void Update () {
        transform.position = new Vector3(target.position.x + offset.x, transform.position.y, target.position.z + offset.z);
	}
}
