﻿using UnityEngine;
using System.Collections;

public abstract class Pickup : MonoBehaviour {

    public AudioClip pickupSnd;

    void Start()
    {
        InvokeRepeating("CheckForPlayer", Time.deltaTime, Time.deltaTime * 5);
    }

    void CheckForPlayer()
    {
        foreach(Collider c in Physics.OverlapSphere(transform.position, 0.8f))
        {
            if(c.gameObject.layer == LayerMask.NameToLayer("Player"))
            {
                PickUp();
                if (pickupSnd) Service.sfxManager.PlaySFX(pickupSnd);
                Destroy(gameObject);
            }
        }
	}

    protected abstract void PickUp();



}
