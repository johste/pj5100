﻿using UnityEngine;
using System.Collections;

public class Tips : Pickup {

    public GameObject tips;

    override protected void PickUp()
    {
        tips.SetActive(true);
        Destroy(gameObject);
    }
}
