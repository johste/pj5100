﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TranquilizersLeft : MonoBehaviour {

    private Text text;

	// Use this for initialization
	void Start () {
	    text = GetComponentInChildren<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	    text.text = GameMaster.tranquilizersLeft + "";
	}
}
