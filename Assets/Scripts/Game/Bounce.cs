﻿using UnityEngine;
using System.Collections;

public class Bounce : MonoBehaviour {

    public float amount = 1;
    public float speed = 1;

    private float defaultY;

    private void Start()
    {
        defaultY = transform.position.y;
    }

	void Update ()
    {
        transform.position = new Vector3(transform.position.x, defaultY + Mathf.Sin(Time.time * speed) * amount, transform.position.z);
    }
}
