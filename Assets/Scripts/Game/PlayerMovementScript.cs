﻿using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour {

    public float movementSpeed = 0.2f;
    public float jumpHeight = 2f;
    public LayerMask unwalkableTerrain;
    public AudioClip[] jumpSnd;
    [HideInInspector]
    public bool isHidden;
    [HideInInspector]
    public bool hideModel;

    private GameMaster gM;
    private MeshRenderer mR;

    private int currentJumpSnd;
    private float squareSize;
    private Vector3 startPos;
    private Vector3 targetPosition;
    private bool swipeHasEnded;
    private bool isMoving;
    private bool canMoveAgain = true;
    private float point;

    private BoxCollider box;
    private bool animTriggered;

    void Awake()
    {
        gM = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        box = GetComponent<BoxCollider>();
        mR = GetComponentInChildren<MeshRenderer>();
    }

    void Start()
    {
        squareSize = GameObject.Find("Grid").GetComponent<VectorGrid>().squareSize;
    }
	
	void Update ()
    {
        Movement();

        box.enabled = !isHidden;

        mR.enabled = !hideModel;

    }

    private void PrepareMovement()
    {
        gM.TriggerOnTimeTick();
        animTriggered = false;
        startPos = transform.position;
        point = 0f;
        isMoving = true;

        currentJumpSnd++;
        if (currentJumpSnd >= jumpSnd.Length) currentJumpSnd = 0;

        Service.sfxManager.PlaySFX(jumpSnd[currentJumpSnd],1,Random.Range(0.7f,1.3f));
    }

    private void SetCanMoveAgain(bool b) {
        canMoveAgain = b;
    }

    public void SkipTurn()
    {
        gM.CancelInvoke("TriggerOnLateTimeTick");
        gM.Invoke("TriggerOnLateTimeTick", movementSpeed);
        gM.TriggerOnTimeTick();
    }

    private void KeyboardInput()
    {
        if (Input.GetKey(KeyCode.W))
            MoveUp();

        if (Input.GetKey(KeyCode.A))
            MoveLeft();

        if (Input.GetKey(KeyCode.S))
            MoveDown();

        if (Input.GetKey(KeyCode.D))
            MoveRight();

        if (Input.GetButtonDown("Jump"))
            SkipTurn();
    }

    private void Movement()
    {
        if (!isMoving)
        {
            if(GameMaster.canMove && canMoveAgain)
                KeyboardInput();
        }
        else
        {
            if (Vector3.Distance(transform.position, targetPosition) < 0.05f)
            {
                transform.position = targetPosition;
                isMoving = false;
                if (!animTriggered)
                {
                    //Debug.Log("Hello");
                    animTriggered = true;
                }
                isHidden = false;
                gM.TriggerOnLateTimeTick();
            }
            else
            {                
                if(point < 1f)
                    point += Time.deltaTime / (movementSpeed / 2f);
                else
                    point = 1f;                      

                Vector3 position = GetQuadraticBezierJumpCurve(startPos, Vector3.Lerp(startPos, targetPosition, 0.5f) + Vector3.up * jumpHeight, targetPosition, point);
                transform.position = position;
                transform.localScale = Vector3.one + new Vector3( -(position.y/2f), position.y/2f, -(position.y / 2f) );
            }       
        }
    }

    private Vector3 GetQuadraticBezierJumpCurve(Vector3 startPosition, Vector3 controlPoint, Vector3 targetpositon, float time)
    {
        var x = Mathf.Pow(1 - time, 2) * startPosition.x + 2 * (1 - time) * time * controlPoint.x + Mathf.Pow(time, 2) * targetpositon.x;
        var y = Mathf.Pow(1 - time, 2) * startPosition.y + 2 * (1 - time) * time * controlPoint.y + Mathf.Pow(time, 2) * targetpositon.y;
        var z = Mathf.Pow(1 - time, 2) * startPosition.z + 2 * (1 - time) * time * controlPoint.z + Mathf.Pow(time, 2) * targetpositon.z;
        return new Vector3 (x, y, z);
    }

    private void OnSwipe(Gesture gesture)
    {
        if (!isMoving && swipeHasEnded && GameMaster.canMove)
        {
            if (gesture.swipe == EasyTouch.SwipeDirection.Up)
                MoveUp();

            if (gesture.swipe == EasyTouch.SwipeDirection.Left)
                MoveLeft();

            if (gesture.swipe == EasyTouch.SwipeDirection.Down)
                MoveDown();

            if (gesture.swipe == EasyTouch.SwipeDirection.Right)
                MoveRight();

            swipeHasEnded = false;
        }
    }

    private void OnSwipeEnd(Gesture gesture)
    {
        swipeHasEnded = true;
    }

    private void MoveUp()
    {
        RaycastHit hit;
        if (!Physics.Raycast(transform.position, Vector3.forward, out hit, squareSize, unwalkableTerrain))
        {
            PrepareMovement();
            targetPosition = transform.position + Vector3.forward * squareSize;
            transform.eulerAngles = new Vector3(0f, 180f, 0f);
        }
        
    }

    private void MoveLeft()
    {
        RaycastHit hit;
        if (!Physics.Raycast(transform.position, Vector3.left, out hit, squareSize, unwalkableTerrain))
        {
            PrepareMovement();
            targetPosition = transform.position + Vector3.left * squareSize;
            transform.eulerAngles = new Vector3(0f, 90f, 0f);
        }
        
    }

    private void MoveDown()
    {
        RaycastHit hit;
        if (!Physics.Raycast(transform.position, Vector3.back, out hit, squareSize, unwalkableTerrain))
        {
            PrepareMovement();
            targetPosition = transform.position + Vector3.back * squareSize;
            transform.eulerAngles = new Vector3(0f, 0f, 0f);
        }
       
    }

    private void MoveRight()
    {
        RaycastHit hit;
        if (!Physics.Raycast(transform.position, Vector3.right, out hit, squareSize, unwalkableTerrain))
        {
            PrepareMovement();
            targetPosition = transform.position + Vector3.right * squareSize;
            transform.eulerAngles = new Vector3(0f, 270f, 0f);
        }
        
    }

    private void OnEnable()
    {
        EasyTouch.On_Swipe += OnSwipe;
        EasyTouch.On_SwipeEnd += OnSwipeEnd;
        GuardAI.OnBeginTurn += SetCanMoveAgain;
        GuardVision.OnEndTurn += SetCanMoveAgain;
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    private void OnDestroy()
    {
        Unsubscribe();
    }

    private void Unsubscribe()
    {
        EasyTouch.On_Swipe -= OnSwipe;
        EasyTouch.On_SwipeEnd -= OnSwipeEnd;
        GuardAI.OnBeginTurn -= SetCanMoveAgain;
        GuardVision.OnEndTurn -= SetCanMoveAgain;
    }
}
