﻿using UnityEngine;
using System.Collections;

public class ScreenFader : MonoBehaviour {

    public float timeToFade;

    private float t;
    public Material m;
    private bool isOpen;

    public float delayTime;

    private void Awake()
    {
        //m = GetComponent<Material>();
        t = 0f;
        m.SetFloat("_Cutoff", t);
        Invoke("OpenBlinds", delayTime);
    }

    private void Update()
    {
        if (t < 1f && isOpen) t += (Time.deltaTime / timeToFade);
        if (t >= 0f && !isOpen) t -= (Time.deltaTime / timeToFade);

        m.SetFloat("_Cutoff", t);
    }



    public void CloseBlinds()
    {
        isOpen = false;
    }

    public void OpenBlinds()
    {
        isOpen = true;
    }
}
