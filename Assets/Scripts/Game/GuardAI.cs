﻿using UnityEngine;
using System.Collections;
using Pathfinding;


public class GuardAI : MonoBehaviour {
    
    public delegate void BeginTurn(bool b);
    public static event BeginTurn OnBeginTurn;

    public Transform target;
    public Transform[] patrolPath;

    private bool canMove = true;
    private PlayerMovementScript pms;
    private float movementSpeed = 0.2f;
    private float jumpHeight = 8f;

    private Vector3 targetPosition;
    private Path path;
    private float nextWaypointDistance = 1;
    private int currentWaypoint = 0;
    private Seeker seeker;
    private float squareSize;
    private Vector3 startPos;
    private bool isMoving;
    private float point;

    private int currentPatrolPoint;

    void Awake()
    {
        seeker = GetComponent<Seeker>();
        squareSize = GameObject.Find("Grid").GetComponent<VectorGrid>().squareSize;
    }

    void Start()
    {
        pms = Service.Connect("Player").GetComponent<PlayerMovementScript>();
        movementSpeed = pms.movementSpeed;
        jumpHeight = pms.jumpHeight;
        UpdatePath();
    }

    void UpdatePath()
    {
        if(target != null)
            seeker.StartPath(transform.position, target.position, OnPathComplete);

        if (patrolPath.Length > 0)
            if(path == null || Vector3.Distance(transform.position, patrolPath[currentPatrolPoint].position) < 0.05f)
                FindNextPatrolPoint();
        //else
            //Debug.Log("Guarding.");
    }

    private void FindNextPatrolPoint()
    {
        currentWaypoint = 0;
        currentPatrolPoint++;
        if (currentPatrolPoint >= patrolPath.Length) currentPatrolPoint = 0;
        seeker.StartPath(transform.position, patrolPath[currentPatrolPoint].position, OnPathComplete);
        
    }

    private void OnPathComplete(Path p)
    {
        //Debug.Log("Yay, we got a path back. Did it have an error? " + p.error);
        if (!p.error)
        {
            path = p;

            //Reset the waypoint counter
            currentWaypoint = 1;
        }
    }

    private void Movement()
    {  

        if (isMoving && path != null)
        {
                //Snap to nearest tile
                if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < 0.05f)
                {
                    transform.position = path.vectorPath[currentWaypoint];
                    isMoving = false;
                    currentWaypoint++;
                    UpdatePath();
                }
                else
                {
                    if (point < 1f)
                        point += Time.deltaTime / (movementSpeed / 2f);
                    else
                        point = 1f;


                    Vector3 position = GetQuadraticBezierJumpCurve(startPos, Vector3.Lerp(startPos, path.vectorPath[currentWaypoint], 0.5f) + Vector3.up * jumpHeight, path.vectorPath[currentWaypoint], point);
                    transform.position = position;
                    transform.localScale = Vector3.one + new Vector3(-(position.y / 4f), position.y / 4f, -(position.y / 4f));
                }
        }
    }

    private void FaceDirection()
    {
        if (path != null && currentWaypoint < path.vectorPath.Count)
        { 
            float offset = 0.5f;
            if (path.vectorPath[currentWaypoint].x > transform.position.x + offset)
            {
                transform.eulerAngles = new Vector3(0f, 270f, 0f);
            }
            else if (path.vectorPath[currentWaypoint].x < transform.position.x - offset)
            {
                transform.eulerAngles = new Vector3(0f, 90f, 0f);
            }
            else if (path.vectorPath[currentWaypoint].z > transform.position.z + offset)
            {
                transform.eulerAngles = new Vector3(0f, 180f, 0f);
            }
            else if (path.vectorPath[currentWaypoint].z < transform.position.z - offset)
            {
                transform.eulerAngles = new Vector3(0f, 0f, 0f);
            }
        }
    }

    private void Update()
    {
        Movement();
    }
    
    private void PrepareMovement()
    {
        startPos = transform.position;
        point = 0f;
        isMoving = true;
        FaceDirection();
    }

    private Vector3 GetQuadraticBezierJumpCurve(Vector3 startPosition, Vector3 controlPoint, Vector3 targetpositon, float time)
    {
        var x = Mathf.Pow(1 - time, 2) * startPosition.x + 2 * (1 - time) * time * controlPoint.x + Mathf.Pow(time, 2) * targetpositon.x;
        var y = Mathf.Pow(1 - time, 2) * startPosition.y + 2 * (1 - time) * time * controlPoint.y + Mathf.Pow(time, 2) * targetpositon.y;
        var z = Mathf.Pow(1 - time, 2) * startPosition.z + 2 * (1 - time) * time * controlPoint.z + Mathf.Pow(time, 2) * targetpositon.z;
        return new Vector3(x, y, z);
    }

    private void OnTimeTick()
    {

        OnBeginTurn(false);

        if (target != null && patrolPath.Length > 0) {
            Debug.LogWarning("Our poor guard is confused. Have you told that poor guard to follow player and a patrol path at the same time?");
            return;
        } else if (canMove) { 
            PrepareMovement();
        }
    }

    public void CanMove(bool b) {
        canMove = b;
    }

    private void OnEnable()
    {
        GameMaster.OnTimeTick += OnTimeTick;
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    private void OnDestroy()
    {
        Unsubscribe();
    }

    private void Unsubscribe()
    {
        GameMaster.OnTimeTick -= OnTimeTick;
    }

}
