﻿using UnityEngine;
using System.Collections;

public class GuardVision : MonoBehaviour {

    public AudioClip[] alertSfx;
    public LayerMask whatIsSeen;
    public LayerMask whatIsPlayer;
    public GameObject exclamationMark;
    public GameObject[] SightTilesArray;


    //  public delegate void SightTileDelegate(int index);
    //  public static SightTileDelegate SightTileOffEvent;
    //  public static SightTileDelegate SightTileOnEvent;

    private int squreSize = 2;
    private int maxSightLength = 3;
    private int sightLength;
    private bool canSee = true;

    Ray leftRay;
    Ray rightRay;
    Ray frontLeftRay;
    Ray frontCenterRay;
    Ray frontRightRay;

    // Ga alle vektorene ein deault value sånn at eg kan bruka de i Raycastingen etter if statementet
    Vector3 offsetLeft = Vector3.zero;
    Vector3 offsetRight = Vector3.zero;
    Vector3 offsetFrontCenter = Vector3.zero;
    Vector3 offsetFrontLeft = Vector3.zero;
    Vector3 offsetFrontRight = Vector3.zero;

    Vector3 dirLeft = Vector3.zero;
    Vector3 dirRight = Vector3.zero;
    Vector3 dirFront = Vector3.zero;

    void Start() {
        Hindsight();
    }
    /*
    void Update() {
        Debug.DrawRay(transform.position + offsetLeft, dirLeft * 2);
        Debug.DrawRay(transform.position + offsetRight, dirRight * 2);
        Debug.DrawRay(transform.position + offsetFrontLeft, dirFront * 2 * sightLength + dirFront / 2);
        Debug.DrawRay(transform.position + offsetFrontCenter, dirFront * 2 * sightLength + dirFront / 2);
        Debug.DrawRay(transform.position + offsetFrontRight, dirFront * 2 * sightLength + dirFront / 2);
       
}
 */
    private void OnTimeTick() {
        for (int i = 0; i < 11; i++) {
            SightTileOnOff(i, false);
        }
    }

    private void OnLateTimeTick() {
        Invoke("Hindsight",Time.deltaTime);
    }

    private void Hindsight() {
        //     Debug.Log("Start vision check");
        //Makes all vision tiles visable
        for (int i = 0; i < 11; i++) {
            SightTileOnOff(i, true);
        }
    
        sightLength = maxSightLength;

        if (!canSee) return;
        
        // And you think my static variables are ugly?!
        // I wanted Draw.Ray to look pretty

        // Facing Down
	    if (transform.eulerAngles.y > -5 && transform.eulerAngles.y < 5) {
            //   Debug.Log("FACING DOWN");
            offsetFrontLeft = new Vector3(squreSize, 0, 0.5f);
            offsetFrontRight = new Vector3(-squreSize, 0, 0.5f);
            offsetFrontCenter = new Vector3(0, 0, 0.5f);
	    }
	    //Facing Left
	    else if (transform.eulerAngles.y > 85 && transform.eulerAngles.y < 95) {
	        //   Debug.Log("FACING RIGHT");
	        offsetFrontLeft = new Vector3(0.5f, 0, -squreSize);
	        offsetFrontRight = new Vector3(0.5f, 0, squreSize);
            offsetFrontCenter = new Vector3(0.5f, 0, 0);
	    }
	    //Facing Up
	    else if (transform.eulerAngles.y > 175 && transform.eulerAngles.y < 185) {
	        //   Debug.Log("FACING UP");
	        offsetFrontLeft = new Vector3(-squreSize, 0, -0.5f);
	        offsetFrontRight = new Vector3(squreSize, 0, -0.5f);
            offsetFrontCenter = new Vector3(0, 0, -0.5f);
	    }
	    // Facing Right
	    else if (transform.eulerAngles.y > 265 && transform.eulerAngles.y < 275) {
	        //   Debug.Log("FACING Right");
	        offsetFrontLeft = new Vector3(-0.5f, 0, squreSize);
	        offsetFrontRight = new Vector3(-0.5f, 0, -squreSize);
            offsetFrontCenter = new Vector3(-0.5f, 0, 0);
	    }

        dirLeft = transform.right;
        dirRight = -transform.right;
        dirFront = -transform.forward;

        leftRay = new Ray(transform.position + offsetLeft, dirLeft);
	    rightRay = new Ray(transform.position + offsetRight, dirRight);
	    frontLeftRay = new Ray(transform.position + offsetFrontLeft, dirFront);
	    frontCenterRay = new Ray(transform.position + offsetFrontCenter, dirFront);
	    frontRightRay = new Ray(transform.position + offsetFrontRight, dirFront);
        
        
	    RaycastHit hitLeft;
	    RaycastHit hitRight;
	    RaycastHit hitFrontLeft;
	    RaycastHit hitFrontCenter;
	    RaycastHit hitFrontRight;

	    if (Physics.Raycast(leftRay, out hitLeft, squreSize, whatIsSeen)) {
            SightTileOnOff(0, false);

            if (hitLeft.collider.gameObject.layer == LayerMask.NameToLayer("Player")) {
	            WasSeen();
	        }

        //    Debug.Log("Object Left hit: " + hitLeft.collider.name);
        }
	    if (Physics.Raycast(rightRay, out hitRight, squreSize, whatIsSeen)) {
            SightTileOnOff(7, false);

            if (hitRight.collider.gameObject.layer == LayerMask.NameToLayer("Player")) {
	            WasSeen();
            }
            //     Debug.Log("Object Right hit: " + hitRight.collider.name);
        }
        if (Physics.Raycast(frontCenterRay, out hitFrontCenter, squreSize * sightLength + 0.5f, whatIsSeen)) {
           // Debug.Log("frontCenterRay hit " + hitFrontCenter.collider.name);

            
            if (hitFrontCenter.collider.gameObject.layer == LayerMask.NameToLayer("Wall") || hitFrontCenter.collider.gameObject.layer == LayerMask.NameToLayer("HidingSpot")) {

                //Debug.Log("hitFrontCenter distance: " + hitFrontCenter.distance);
                SightTileOnOff(1, false);
                SightTileOnOff(2, false);
                SightTileOnOff(3, false);
                SightTileOnOff(4, false);
                SightTileOnOff(5, false);
                SightTileOnOff(6, false);
                SightTileOnOff(8, false);
                SightTileOnOff(9, false);
                SightTileOnOff(10, false);
            
                if (hitFrontCenter.distance < 2.5f) {

                    sightLength = 0;

                    //Debug.Log("Reducing sight length by 2");
                    //Debug.Log("1 square away: " + hitFrontCenter.distance);

                } else if (hitFrontCenter.distance > 2.5f && hitFrontCenter.distance < 4.5f) {

                    sightLength = 1;

                    SightTileOnOff(1, true);
                    SightTileOnOff(4, true);
                    SightTileOnOff(8, true);
                    //Debug.Log("Reducing sight length by 1");
                          //Debug.Log("hitFrontCenter 2 squares away: " + hitFrontCenter.distance);
                } else if (hitFrontCenter.distance > 4.5f) {

                    sightLength = 2;

                    SightTileOnOff(1, true);
                    SightTileOnOff(2, true);
                    SightTileOnOff(4, true);
                    SightTileOnOff(5, true);
                    SightTileOnOff(8, true);
                    SightTileOnOff(9, true);
                //    Debug.Log("hitFrontCenter 3 squares away: " + hitFrontCenter.distance);
                }
            }
            if (hitFrontCenter.collider.gameObject.layer == LayerMask.NameToLayer("Player")) {
                WasSeen();
            }

            //       Debug.Log("Object Front Center hit: " + hitFrontCenter.collider.name);
        }
        if (Physics.Raycast(frontLeftRay, out hitFrontLeft, squreSize * sightLength + 0.5f, whatIsSeen)) {


            if (hitFrontLeft.collider.gameObject.layer == LayerMask.NameToLayer("Wall") || hitFrontLeft.collider.gameObject.layer == LayerMask.NameToLayer("HidingSpot")) {
                SightTileOnOff(1, false);
                SightTileOnOff(2, false);
                SightTileOnOff(3, false);

                if (hitFrontLeft.distance > 2.5f && hitFrontLeft.distance < 4.5f) {

                    SightTileOnOff(1, true);
                    //Debug.Log("hitFrontLeft 2 squares away: " + hitFrontLeft.distance);
                } else if (hitFrontLeft.distance > 4.5f) {
                    SightTileOnOff(1, true);
                    SightTileOnOff(2, true);
                        //Debug.Log("hitFrontLeft 3 squares away: " + hitFrontLeft.distance);
                }
            }

            if (hitFrontLeft.collider.gameObject.layer == LayerMask.NameToLayer("Player")) {
                WasSeen();
            }

                  //Debug.Log("Object Front Left hit: " + hitFrontLeft.collider.name);
        }
        if (Physics.Raycast(frontRightRay, out hitFrontRight, squreSize * sightLength + 0.5f, whatIsSeen)) {


            if (hitFrontRight.collider.gameObject.layer == LayerMask.NameToLayer("Wall") || hitFrontRight.collider.gameObject.layer == LayerMask.NameToLayer("HidingSpot")) {
                SightTileOnOff(8, false);
                SightTileOnOff(9, false);
                SightTileOnOff(10, false);

                if (hitFrontRight.distance > 2.5f && hitFrontRight.distance < 4.5f) {
                    SightTileOnOff(8, true);
                         //Debug.Log("hitFrontRight 2 squares away: " + hitFrontRight.distance);
                }
                if (hitFrontRight.distance > 4.5f) {
                    SightTileOnOff(8, true);
                    SightTileOnOff(9, true);
                          //Debug.Log("hitFrontRight 3 squares away: " + hitFrontRight.distance);
                }
            }

            if (hitFrontRight.collider.gameObject.layer == LayerMask.NameToLayer("Player")) {
                WasSeen();
            }


            //       Debug.Log("Object Front Right hit: " + hitFrontRight.collider.name);
        }

        //Johannes added this to avoid hiding grandma inside bear guts.
        CheckIfPlayerInsideBear();

        //     Debug.Log("End vision check");
        /*
        Debug.DrawRay(transform.position + offsetLeft, dirLeft * 2);
        Debug.DrawRay(transform.position + offsetRight, dirRight * 2);
        Debug.DrawRay(transform.position + offsetFrontLeft, dirFront * 2 * sightLength + dirFront / 2);
        Debug.DrawRay(transform.position + offsetFrontCenter, dirFront * 2 * sightLength + dirFront / 2);
        Debug.DrawRay(transform.position + offsetFrontRight, dirFront * 6 * sightLength + dirFront / 2);
        */
        OnEndTurn(true);
    }

    public delegate void EndTurn(bool b);
    public static EndTurn OnEndTurn;

    private void CheckIfPlayerInsideBear() {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 1f);
        foreach(Collider c in hitColliders) {
            if(c.gameObject.layer == LayerMask.NameToLayer("Player"))
            {
                WasSeen();
            }
        }
    }

    private void WasSeen() {
        Service.sfxManager.PlaySFX(alertSfx[Random.Range(0,alertSfx.Length)]);
        exclamationMark.SetActive(true);
        GameMaster.current.WasSeen();
     //   CanSee(false);
    }

    public void CanSee(bool b) {
        canSee = b;
        
        for (int i = 0; i < 11; i++) {
            SightTileOnOff(i, false);
        }

    }

    void SightTileOnOff(int i, bool isOn) {
        if (SightTilesArray.Length > 0) {
            if (!canSee) {
                SightTilesArray[i].GetComponent<MeshRenderer>().enabled = false;
            } else {
                SightTilesArray[i].GetComponent<MeshRenderer>().enabled = isOn;
            }
        }
        
    }

    private void OnEnable() {
        GameMaster.OnLateTimeTick += OnLateTimeTick;
        GameMaster.OnTimeTick += OnTimeTick;
    }

    private void OnDisable()  {
        Unsubscribe();
    }

    private void OnDestroy() {
        Unsubscribe();
    }

    private void Unsubscribe() {
        GameMaster.OnLateTimeTick -= OnLateTimeTick;
        GameMaster.OnTimeTick -= OnTimeTick;
    }
}

