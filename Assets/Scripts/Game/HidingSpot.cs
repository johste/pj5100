﻿using UnityEngine;
using System.Collections;

public class HidingSpot : MonoBehaviour {

    public GameObject alternateObj;
    public Material defaultMat;
    public Material invisibleMat;
    public float sphereRadius = 1f;
    public LayerMask whatIsPlayer;
    public AudioClip hideSFX;
    public bool hideModel;

    private Renderer r;
    private float alpha = 1f;
    private bool isInside;
    private PlayerMovementScript pms;

    private void Awake()
    {
        r = GetComponentInChildren<Renderer>();
        OnLateTimeTick();
    }

    private void Start()
    {
        pms = Service.Connect("Player").GetComponent<PlayerMovementScript>();
    }

	void OnLateTimeTick()
    {
        if(Physics.CheckSphere(transform.position, sphereRadius, whatIsPlayer))
        {
            if (alternateObj == null)
            {
                r.material = invisibleMat;
            }
            else
            {
                alternateObj.SetActive(true);
                r.enabled = false;
            }
                
            isInside = true;
            if (!pms.isHidden)
            {
                pms.isHidden = true;
                if(hideModel)pms.hideModel = true;
                Service.sfxManager.PlaySFX(hideSFX);
            }
            
        }
        else
        {
            if (pms != null && !pms.isHidden)
            {
                if(alternateObj == null)
                    r.material = defaultMat;
                else
                {
                    alternateObj.SetActive(false);
                    r.enabled = true;
                }
                isInside = false;
                pms.hideModel = false;
            }
        }
	}

    void Update()
    {
        if (alternateObj == null)
        {

        if (isInside && alpha > 85f/255f) alpha = 0.3f;

        if (!isInside && alpha < 1f) alpha += Time.deltaTime * 3;
        else if(!isInside && alpha >= 1f) r.material = defaultMat;

        r.material.color = new Color(r.material.color.r, r.material.color.g, r.material.color.b, alpha);

        }
    }

    private void OnEnable()
    {
        GameMaster.OnLateTimeTick += OnLateTimeTick;
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    private void OnDestroy()
    {
        Unsubscribe();
    }

    private void Unsubscribe()
    {
        GameMaster.OnLateTimeTick -= OnLateTimeTick;
    }
}
