﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MuteButton : MonoBehaviour {

	void Awake ()
    {
        gameObject.SetActive(PlayerPrefs.GetInt("IsMute") == 0);
    }
	
}
