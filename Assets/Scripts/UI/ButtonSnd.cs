﻿using UnityEngine;
using System.Collections;

public class ButtonSnd : MonoBehaviour {

	public void OnClick ()
    {
        if(MusicManager.current != null)
            Service.sfxManager.PlaySFX(MusicManager.btnSfxSingleton);
	}
}
