﻿using UnityEngine;
using System.Collections;

public class HasPlayerFinishedGame : MonoBehaviour {

    public GameObject granny;
    public GameObject mountain;
	
	void Start ()
    {
        if(Service.save.GetValue<int>("LevelsUnlocked") >= 24)
        {
            mountain.SetActive(true);
        }
        else
        {
            granny.SetActive(true);
        }
        
	}
}
