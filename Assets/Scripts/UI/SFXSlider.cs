﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SFXSlider : MonoBehaviour {

    Slider slider;
	void Awake ()
    {   
        slider = GetComponent<Slider>();
        if (PlayerPrefs.GetInt("FirstBootSFX") == 0)
        {
            OnChange();
            PlayerPrefs.SetInt("FirstBootSFX", 1); 
        }
            slider.value = PlayerPrefs.GetFloat("sfxVolume");
    }
	
	public void OnChange ()
    {
        PlayerPrefs.SetFloat("sfxVolume", slider.value); 
    }
}
