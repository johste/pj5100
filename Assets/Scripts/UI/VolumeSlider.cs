﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class VolumeSlider : MonoBehaviour {

    Slider slider;
    void Awake()
    {
        slider = GetComponent<Slider>();
        if (PlayerPrefs.GetInt("FirstBootVolume") == 0)
        {
            OnChange();
            PlayerPrefs.SetInt("FirstBootVolume", 1);
        }
        slider.value = PlayerPrefs.GetFloat("volume");
    }

    public void OnChange()
    {
        PlayerPrefs.SetFloat("volume", slider.value);
    }
}
