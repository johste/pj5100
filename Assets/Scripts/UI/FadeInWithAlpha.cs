﻿using UnityEngine;
using System.Collections;

public class FadeInWithAlpha : MonoBehaviour {

    private CanvasGroup cG;
    private bool ready;

    private void Awake()
    {
        cG = GetComponent<CanvasGroup>();
        FadeIn(1f);
    }

    public void FadeIn(float delay)
    {
        ready = false;
        cG.alpha = 0f;
        Invoke("StartFadeIn", delay);
    }

    void Update()
    {
        if (ready)
        {
            cG.alpha += Time.deltaTime;
            if (cG.alpha >= 1f) ready = false;
        }
            
    }

    void StartFadeIn()
    {
        ready = true;
    }

}
