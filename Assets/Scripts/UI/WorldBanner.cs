﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorldBanner : MonoBehaviour {

    public string worldName;
    public Text text;

    private bool flyAway;
    private CanvasGroup cG;

	void Awake ()
    {
        cG = GetComponent<CanvasGroup>();
        text.text = worldName;
        Invoke("FlyAway", 4f);
        iTween.PunchPosition(gameObject, new Vector3(0f, 100f, 0f), 15f);
    }
	
	
	void FlyAway()
    {
        flyAway = true;
        Invoke("DestroyMe",5f);
    }

    void Update()
    {
        if (flyAway)
            cG.alpha -= Time.deltaTime;
            //gameObject.transform.position += new Vector3(0f, Time.deltaTime * 1000f, 0f);
    }

    void DestroyMe()
    {
        Destroy(gameObject);
    }
}
