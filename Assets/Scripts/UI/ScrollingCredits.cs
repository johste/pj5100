﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ScrollingCredits : MonoBehaviour {

    public RectTransform creditsRoot;
    public float scrollingSpeed;
    public float startValue;
    public float endValue;
    public FadeInWithAlpha Canvas;

    public GameObject mainMenu;
    public ScreenFader blinds;

    private CanvasGroup cG;
    private float t;
    private float alpha = 1;
    private bool done;

    private void Awake()
    {
        cG = GetComponent<CanvasGroup>();
    }

	private void Update ()
    {
        if (!done)
        {
            t += Time.deltaTime / scrollingSpeed;
            creditsRoot.localPosition = new Vector3(0f, Mathf.Lerp(startValue, endValue, t), 0f);
            if (t >= 1f)
            {
                Invoke("Finished", 3f);
                done = true;
            }
        }
        else
        {
            alpha -= Time.deltaTime / 2.5f;
            cG.alpha = alpha;
        }
    }

    private void Finished()
    {
        gameObject.SetActive(false);
    }


    private void OnDisable()
    {
        Reset();
    }

    private void Reset()
    {
        mainMenu.SetActive(true);
        t = 0;
        done = false;
        alpha = 1f;
        cG.alpha = alpha;
        creditsRoot.localPosition = new Vector3(0f, startValue, 0f);
        blinds.OpenBlinds();
        Canvas.FadeIn(1f);
    }
}
