﻿using UnityEngine;
using System.Collections;

public class UnMuteButton : MonoBehaviour {

    void Awake()
    {
        gameObject.SetActive(PlayerPrefs.GetInt("IsMute") == 1);
    }

}
