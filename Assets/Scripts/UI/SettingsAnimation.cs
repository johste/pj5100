﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsAnimation : MonoBehaviour {

    public GameObject settingsBtn;

    private bool finished;
    private bool close;
    private CanvasGroup cG;

    void Awake()
    {
        cG = GetComponent<CanvasGroup>();
    }

	void OnEnable ()
    {
        transform.localScale = Vector3.zero;
        cG.alpha = 0f;
        finished = false;
        close = false;
    }
	

	void Update ()
    {
        if (!close)
        {
	        if(transform.localScale.magnitude <= Vector3.one.magnitude && !finished)
            {
                transform.localScale = Vector3.Lerp(transform.localScale,Vector3.one,0.3f);
                cG.alpha = Mathf.Lerp(cG.alpha, 1f, 0.3f);
            }
            else
            {
                finished = true;
            }

        }
        else
        {
            if (transform.localScale.magnitude > 0.2f) 
            {
                transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, 0.3f);
                cG.alpha = Mathf.Lerp(cG.alpha, 0f, 0.3f);
            }
            else
            {
                settingsBtn.SetActive(true);
                transform.parent.gameObject.SetActive(false);
            }
        }

      
    }

    public void Close()
    {
        close = true;
    }

}
