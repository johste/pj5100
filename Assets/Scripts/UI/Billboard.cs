﻿using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviour {


    private Transform player;
    public float damping = 1f;

    void Start ()
    {
        player = Service.Connect("Player").transform;
	}
	
	void Update ()
    {
        var lookPos = player.position - transform.position;
        lookPos.y = 0;
        var rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
    }
}
