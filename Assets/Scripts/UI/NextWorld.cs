﻿using UnityEngine;
using System.Collections;

public class NextWorld : MonoBehaviour {

    public int treshhold;

	void Start ()
    {
        if(Service.save.GetValue<int>("LevelsUnlocked") < treshhold)
        {
            gameObject.SetActive(false);
        }
	}
}
