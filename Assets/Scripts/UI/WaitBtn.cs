﻿using UnityEngine;
using System.Collections;

public class WaitBtn : MonoBehaviour {

    PlayerMovementScript pms;

	private void Start ()
    {
        try
        {
            pms = Service.Connect("Player").GetComponent<PlayerMovementScript>();
        }
        catch
        {
            Debug.LogWarning("Can't find Player or Player Movement Script is not attached");
        }
	}
	
    public void OnClick()
    {
        if (pms) pms.SkipTurn();
    }
}
