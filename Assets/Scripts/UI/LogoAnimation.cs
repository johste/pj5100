﻿using UnityEngine;
using System.Collections;

public class LogoAnimation : MonoBehaviour {

	
	void Start ()
    {
        iTween.PunchRotation(gameObject, new Vector3(0f, 0f, 360f), 2f);
        iTween.PunchScale(gameObject, new Vector3(10f, 10f, 10f), 2f);
	}
}
