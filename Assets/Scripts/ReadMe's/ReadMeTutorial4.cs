﻿using UnityEngine;
using System.Collections;

public class ReadMeTutorial4 : MonoBehaviour {

    /*
    This level needs some events that triggers a UI element with the text:
    "Sometimes there is a bear in your path that proves to hard to get past.
    In those times you can use a tranquilizer-dart to put it to sleep.
    Sleeping bears cannot discover you, but they only sleep for 3 rounds, 
    so be careful when you use them."

    Then we need a second prompt that says:
    "Remember, you only have a limited amount of tranquilizer-darts.
    Use them wisely!"
    */
}
