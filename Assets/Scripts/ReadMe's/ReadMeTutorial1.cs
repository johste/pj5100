﻿using UnityEngine;
using System.Collections;

public class ReadMeTutorial1 : MonoBehaviour {

	/*
    This level needs some events that triggers a UI element with the text:
    "Take care not to be seen by the bear. 
    The highligted squares are the squares the bear can see."

    Then we need a second prompt that says:
    "Walk in to the tiny indent in the road, and push on the hourglass-button to wait. 
    You may have to push multiple times for the bear to pass you."

    And a third one that says:
    "Grab the big bunch of blueberries to finish the level."
    */
}
