﻿using UnityEngine;
using System.Collections;
using Vectrosity;
using System.Collections.Generic;

public class LevelPath : MonoBehaviour {

    public bool onlyDisplayUnlockedLevels;
    public int numberOfUnlockedLevels = 0;
    public int highestLevelInWorld;
    public int currentWorld;

    public GameObject[] levels;
    public Texture lineTexture;
    public float lineWidth = 2;
    public int segments = 20;

    private Vector3[] linePoints;

    [HideInInspector]
    public List<VectorLine> entireRoute;


    private VectorLine myLine;
	void Start ()
    {
        if (MusicManager.current != null) MusicManager.current.SetMusic(currentWorld);
        else Debug.LogWarning("MusicManager == null");
        numberOfUnlockedLevels = Service.save.GetValue<int>("LevelsUnlocked") - (currentWorld * 8);
        entireRoute = new List<VectorLine>();

        //Fill path;
        linePoints = new Vector3[levels.Length];
        for(int i = 0; i < levels.Length; i++)
        {
            linePoints[i] = levels[i].transform.position;
        }

        //myLine = new VectorLine("Road", linePoints, lineMaterial, lineWidth);
        //myLine.MakeCurve(linePoints);
        //myLine.Draw3D();

        for(int i = 0; i < levels.Length-1; i++)
        {
            Vector3 right = Vector3.Cross(Vector3.Lerp(levels[i].transform.position, levels[i + 1].transform.position, 1f).normalized, Vector3.up).normalized;
            List<Vector3> path = new List<Vector3>()
            {
                levels[i].transform.position,
                Vector3.Lerp(levels[i].transform.position, levels[i + 1].transform.position, 0.3f) + (right * 3f),
                levels[i + 1].transform.position,
                Vector3.Lerp(levels[i].transform.position, levels[i + 1].transform.position, 0.7f) - (right * 3f)
            };

            VectorLine newLine = new VectorLine("Curve", new List< Vector3 > (segments + 1), lineTexture, lineWidth, LineType.Continuous, Joins.Weld);
            newLine.continuousTexture = true;
            //VectorLine newLine = new VectorLine("Road" + i, path, lineTexture, lineWidth,LineType.Continuous,Joins.Weld);

            newLine.MakeCurve(path.ToArray());
            entireRoute.Add(newLine);
            if (i >= numberOfUnlockedLevels && onlyDisplayUnlockedLevels)
                return;

            newLine.Draw3D();
        }
	}
	
	void Update () {
        //Vector3 cP = Quaternion.Euler(0, 90f, 0) * Vector3.Lerp ( levels[i].transform.position, levels[i + 1].transform.position, 0.7f );
    }
}
