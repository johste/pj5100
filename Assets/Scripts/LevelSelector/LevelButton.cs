﻿using UnityEngine;
using System.Collections;

public class LevelButton : MonoBehaviour {

    public int buttomNum = 0;
    public int levelToLoad;
    private PlayerLevelMovement pLM;
    private ButtonSnd btnSfx;

	void Start ()
    {
        pLM = Service.Connect("Player").GetComponent<PlayerLevelMovement>();
        btnSfx = GetComponent<ButtonSnd>();
    }

    private void SetTargetLevel(Gesture gesture)
    {
        if(gesture.pickedObject == this.gameObject.GetComponentInChildren<CapsuleCollider>().gameObject)
        {
            pLM.SetClickedLevel(buttomNum, levelToLoad);
            btnSfx.OnClick();
        } 
    }

    private void OnEnable()
    {
        EasyTouch.On_TouchUp += SetTargetLevel;
    }

    private void OnDisable()
    {
        EasyTouch.On_TouchUp -= SetTargetLevel;
    }

    private void OnDestroy()
    {
        EasyTouch.On_TouchUp -= SetTargetLevel;
    }
}
