﻿using UnityEngine;
using System.Collections;

public class LoadSelectedLevelButton : MonoBehaviour {

    public int worldID;

	public void LoadSelected()
    {
        //Debug.Log(Service.Connect("Player").GetComponent<PlayerLevelMovement>().levelToLoad);
        GameMaster.current.GoToLevel(Service.Connect("Player").GetComponent<PlayerLevelMovement>().levelToLoad);
    }

    public void LoadWorld()
    {
        if(worldID != 0)
            GameMaster.current.GoToLevel(worldID);
    }
}
