﻿using UnityEngine;
using System.Collections;

public class LevelSelectCamera : MonoBehaviour {

    public float minYValue;
    public float maxYValue;

    private PlayerLevelMovement player;
    private LevelPath levelPath;

    void Start()
    {
        //Service.save.SetValue("World", 1);
        //Service.save.SetValue("LevelsUnlocked", 10);
        player = Service.Connect("Player").GetComponent<PlayerLevelMovement>();
        levelPath = Service.Connect("Path").GetComponent<LevelPath>();
    }

	void Update ()
    {
        int mod = 1;
        if (player.movingBackwards)
            mod = 0;
        else
            mod = -1;


            //Hvor langt har jeg gått + hvor langt jeg har igjen.
            float t = ((float)(player.targetLevel + mod) / (float)levelPath.levels.Length) + (player.currentSpotNormalized / levelPath.levels.Length);

        transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(minYValue,maxYValue,t));
	}
}
