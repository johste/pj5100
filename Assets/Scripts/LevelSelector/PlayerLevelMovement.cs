﻿using UnityEngine;
using System.Collections;
using Vectrosity;

public class PlayerLevelMovement : MonoBehaviour {

    public LevelPath path;

    [HideInInspector]
    public int currentLevel = 0;
    [HideInInspector]
    public int targetLevel = 0;
    public int levelToLoad = 5;
    private GameObject player;

    private VectorLine currentPath;

    [HideInInspector]
    public float currentSpotNormalized;
    private float currentSpot;
    private bool locationReached;
    private bool firstMove = true;
    private bool swipedPositive;
    private bool swipedNegative;
    [HideInInspector]
    public bool movingBackwards;
    private int clickedLevel;

    void Start ()
    {
        player = Service.Connect("Player");
        path = Service.Connect("Path").GetComponent<LevelPath>();

        //SetClickedLevel(Service.save.GetValue<int>("LevelsUnlocked"));
        /*
        
        firstMove = false;
        currentLevel = targetLevel;
        locationReached = true;
        */
        
        if (Service.save.HasKey("LevelsUnlocked") && Service.save.GetValue<int>("LevelsUnlocked") < path.highestLevelInWorld)
        {
            //Debug.Log(Service.save.GetValue<int>("LevelsUnlocked") - (path.currentWorld * 8));
            transform.position = path.levels[Service.save.GetValue<int>("LevelsUnlocked") - (path.currentWorld * 8)].transform.position;
            currentLevel = Service.save.GetValue<int>("LevelsUnlocked") - (path.currentWorld * 8);
            levelToLoad = currentLevel + 5 + (path.currentWorld * 8);
            targetLevel = currentLevel;
            //Debug.Log(targetLevel + " " + path.entireRoute.Count);
            if (targetLevel < path.entireRoute.Count)
            {
                currentPath = path.entireRoute[targetLevel];
            }
            else
            {
                currentPath = path.entireRoute[targetLevel -1];
            }
            currentSpot = 0f;
            currentSpotNormalized = Mathf.Clamp(currentSpot / currentPath.GetLength(), 0f, 1f);
            locationReached = true;
            firstMove = false;
            swipedPositive = false;
            swipedNegative = false;
            movingBackwards = true;
            clickedLevel = 2;
        }
        
    }

    public void SetLevelToLoad(int level)
    {
        levelToLoad = level;
    }

    private void PrintAll()
    {
        Debug.Log("path " + path);
        Debug.Log("currentLevel " + currentLevel);
        Debug.Log("targetLevel " + targetLevel);
        Debug.Log("player " + player);
        Debug.Log("currentPath " + currentPath);
        Debug.Log("currentSpotNormalized " + currentSpotNormalized);
        Debug.Log("currentSpot " + currentSpot);
        Debug.Log("locationReached " + locationReached);
        Debug.Log("firstMove " + firstMove);
        Debug.Log("swipedPositive " + swipedPositive);
        Debug.Log("swipedNegative " + swipedNegative);
        Debug.Log("movingBackwards " + movingBackwards);
        Debug.Log("clickedLevel " + clickedLevel);
    }

    private void MoveBackwards()
    {
        swipedNegative = false;
        if (targetLevel - 1 < 0) return;
        movingBackwards = true;
        transform.position = path.levels[targetLevel].transform.position;
        firstMove = false;
        targetLevel--;
        currentPath = path.entireRoute[targetLevel];
        currentSpot = path.entireRoute[targetLevel].GetLength();
        locationReached = false;
    }

    private void MoveForward()
    {
        swipedPositive = false;
        if (targetLevel + 1 >= path.levels.Length) return;
        movingBackwards = false;
        transform.position = path.levels[targetLevel].transform.position;
        firstMove = false;
        currentSpot = 0f;
        currentPath = path.entireRoute[targetLevel];
        targetLevel++;
        locationReached = false;
        if (targetLevel >= path.levels.Length - 1) targetLevel = path.levels.Length - 1;
    }

    void Update ()
    {
        if (swipedNegative)
        {
            MoveBackwards();
        }
        else if (swipedPositive)
        {
            MoveForward();
        }

        if (firstMove) return;

        
        if (currentLevel + 1 < path.levels.Length)
            player.transform.LookAt(path.levels[clickedLevel].transform.position);
        else
            player.transform.LookAt(path.levels[0].transform.position);
        
        MoveAlongLine();
    }

    private void OnArrival()
    {
        //Debug.Log("Arrived");
        currentLevel = targetLevel;
        locationReached = true;

        if (currentLevel == clickedLevel)
        {
            //PrintAll();
            return;

        }

        if (clickedLevel > currentLevel)
        {
            SwipedPositive();
        }

        if (clickedLevel < currentLevel)
        {
            SwipedNegative();
        }
    }

    void MoveAlongLine()
    {
        if(path != null)
        {
            if(currentLevel < targetLevel)
            {
                currentSpot += Time.deltaTime * 12f;

                if (currentSpot < currentPath.GetLength())
                {
                    currentSpotNormalized = Mathf.Clamp(currentSpot / currentPath.GetLength(),0f,1f);
                    transform.position = currentPath.GetPoint3D(currentSpot);
                }
                else
                {
                    OnArrival();
                }
            }
            if (currentLevel > targetLevel)
            {
                currentSpot -= Time.deltaTime * 12f;

                if (currentSpot > 0)
                {
                    currentSpotNormalized = Mathf.Clamp(currentSpot / currentPath.GetLength(), 0f, 1f);
                    transform.position = currentPath.GetPoint3D(currentSpot);
                }
                else
                {
                    OnArrival();
                }
            }
        }
    }

    private void SwipedPositive()
    {
        swipedPositive = true;
        movingBackwards = false;
    }

    private void SwipedNegative()
    {
        swipedNegative = true;
        movingBackwards = true;
    }

    public void SetClickedLevel(int i, int level)
    {
        if (!locationReached && !firstMove) return;
        if (i > path.numberOfUnlockedLevels) return;
        clickedLevel = i;
        SetLevelToLoad(level);

        if (clickedLevel > currentLevel)
        {
            SwipedPositive();
        }

        if (clickedLevel < currentLevel)
        {
            SwipedNegative();
        }
       
    }
}
