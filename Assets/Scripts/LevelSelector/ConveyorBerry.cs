﻿using UnityEngine;
using System.Collections;

public class ConveyorBerry : MonoBehaviour {

    public Transform[] patrolPoints;
    public float speed;
    public float startDelay;
    public AudioClip splatSound;
    private Vector3 startPos;
    private int currentPoint;
    private bool started;
    private MeshRenderer mR;
    private float startSize;

    void Awake()
    {
        startSize = transform.localScale.x;
        mR = GetComponent<MeshRenderer>();
        mR.enabled = false;
        transform.localScale = Vector3.zero;
    }

    void Start()
    {
        startPos = transform.position;
        Invoke("Ready", startDelay);
    }

    void Ready()
    {
        started = true;
        mR.enabled = true;
    }

	void Update ()
    {
	    if(patrolPoints.Length > 0 && started)
        {
            if (transform.localScale.x < startSize && !(currentPoint == patrolPoints.Length - 1)) transform.localScale += (Vector3.one * startSize) * (Time.deltaTime * 2f);
            transform.position = Vector3.MoveTowards(transform.position,patrolPoints[currentPoint].position,speed * Time.deltaTime);

            if (currentPoint == patrolPoints.Length - 1)
                transform.localScale -= (Vector3.one * startSize) * (Time.deltaTime * 2f);

            if (Vector3.Distance(transform.position, patrolPoints[currentPoint].position) < 0.1f)
            {
                currentPoint++;
                if (currentPoint >= patrolPoints.Length)
                {
                    transform.position = startPos;
                    Service.sfxManager.PlaySFX(splatSound);
                    currentPoint = 0;
                    transform.localScale = Vector3.zero;
                }
            }

        }
	}
}
