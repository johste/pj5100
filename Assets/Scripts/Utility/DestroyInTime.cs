﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DestroyInTime : MonoBehaviour {

    public float destroyTimer = 0.3f;

	void Start () 
    {
        Invoke ( "DestroyMe", destroyTimer );
	}
	    
	void DestroyMe () 
    {
        Destroy ( gameObject );
	}
  
}
