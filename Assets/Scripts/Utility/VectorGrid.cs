﻿using UnityEngine;
using System.Collections;
using Vectrosity;
using System.Collections.Generic;

public class VectorGrid : MonoBehaviour {

    public float width, height;
    public float squareSize = 1;
    public Texture lineTexture;
    public Vector3 offset;

    public float lineThickness = 2;

    private List<Vector3> linePoints;

    void Awake ()
    {
        //Draw Width
        for(int i = 0; i < (width +1); i++)
        {
            linePoints = new List<Vector3>();

            linePoints.Add(new Vector3( ( width * ( squareSize / 2 ) ) - (i * squareSize) + (squareSize / 2f), offset.y, height * (squareSize / 2) + offset.z));
            linePoints.Add(new Vector3( ( width * ( squareSize / 2 ) ) - (i * squareSize) + (squareSize / 2f), offset.y, -height * (squareSize / 2) + offset.z));

            VectorLine myLine = new VectorLine("MyLine", linePoints, lineTexture, lineThickness, LineType.Discrete);


            myLine.Draw3DAuto();
        }
        
        
        //Draw Height
        for (int i = 0; i < (height +1); i++)
        {
            linePoints = new List<Vector3>();

            linePoints.Add(new Vector3( width * (squareSize / 2) + offset.x, offset.y, ( height * (squareSize / 2) ) - (i * squareSize) + (squareSize / 2f)));
            linePoints.Add(new Vector3( -width * (squareSize / 2) + offset.x, offset.y, ( height * (squareSize / 2) ) - (i * squareSize) + (squareSize / 2f)));

            VectorLine myLine = new VectorLine("MyLine", linePoints, lineTexture, lineThickness, LineType.Discrete);

            myLine.Draw3DAuto();
        }
    }
}
