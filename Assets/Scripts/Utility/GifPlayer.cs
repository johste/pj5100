﻿using UnityEngine;
using System.Collections;

public class GifPlayer : MonoBehaviour {

    public Texture[] frames;
    public float speed;
    
    private int currentFrame;
    public Material m;

	void Start ()
    {
        InvokeRepeating("NextFrame", Time.deltaTime,Time.deltaTime * speed);
    }
	
	void NextFrame ()
    {
        currentFrame++;
        if (currentFrame >= frames.Length) currentFrame = 0;
        m.SetTexture("_MainTex", frames[currentFrame]);
    }
}
