﻿using UnityEngine;
using System.Collections;

public class DelayActivation : MonoBehaviour {

    public float delay;

	void Start ()
    {
        Invoke("SetActive", delay);
	}
	
	
	void SetActive()
    {
        transform.GetChild(0).gameObject.SetActive(true);
	}
}
