﻿using UnityEngine;
using System.Collections;

public class Singleton : MonoBehaviour {

	static Singleton _instance;

	void Awake(){

		if (_instance == null) {
			DontDestroyOnLoad (transform.gameObject);
			_instance = this;
		}
		else
		{
			Destroy(gameObject);
		}
	}
}
