﻿using UnityEngine;
using System.Collections;

public class SFXManager : MonoBehaviour {

    private AudioSource[] audioSources;
    private int currentSource;

	private void Awake ()
    {
        audioSources = GetComponents<AudioSource>();
    }

    public void PlaySFX(AudioClip audioClip)
    {
        PlaySFX(audioClip, 1f, 1f, 128);
    }

    public void PlaySFX(AudioClip audioClip, float volume)
    {
        PlaySFX(audioClip, volume, 1f, 128);
    }

    public void PlaySFX(AudioClip audioClip, float volume, float pitch)
    {
        PlaySFX(audioClip, volume, pitch, 128);
    }

    public void PlaySFX(AudioClip audioClip, float volume, float pitch, int priority)
    {
        if (PlayerPrefs.GetInt("IsMute") == 1) return;
        currentSource++;
        if (currentSource >= audioSources.Length) currentSource = 0;
        audioSources[currentSource].volume = volume * PlayerPrefs.GetFloat("sfxVolume");
        audioSources[currentSource].pitch = pitch;
        audioSources[currentSource].priority = priority;
        audioSources[currentSource].PlayOneShot(audioClip);
    }
}
