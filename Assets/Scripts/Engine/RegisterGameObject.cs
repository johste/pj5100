﻿using UnityEngine;
using System.Collections;

public class RegisterGameObject : MonoBehaviour
{
    public bool disableOnRegister;

	void Awake ()
    {
        Service.Register(gameObject);
        gameObject.SetActive(!disableOnRegister);
	}
}
