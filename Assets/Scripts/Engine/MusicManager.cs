﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

    public AudioClip btnSfx;
    public static AudioClip btnSfxSingleton;
    public AudioClip[] music;

    private AudioSource[] source;
    public static MusicManager current;

	private void Awake ()
    {
        if (current == null)
        {
            DontDestroyOnLoad(transform.gameObject);
            current = this;
        }
        else
        {
            Destroy(gameObject);
        }
        source = GetComponents<AudioSource>();
        btnSfxSingleton = btnSfx;
    }

    private void Start()
    {
        SetMusic(Service.save.GetValue<int>("World"));
        OnMute();
        source[0].volume = PlayerPrefs.GetFloat("volume");
        source[1].volume = PlayerPrefs.GetFloat("sfxVolume");
    }
	
	public void Pause ()
    {
        source[0].Pause();
    }

    public void Continue ()
    {
        source[0].Play();
    }

    public void SetMusic(int world)
    {
        //Debug.Log(world);
        if(source[0].clip != music[world])
        {
            source[0].clip = music[world];
            source[0].Play();
        }
    }

    private void OnMute()
    {
        if (PlayerPrefs.GetInt("IsMute") == 1) Pause();
        else
            Continue();

    }

    private void OnEnable()
    {
        GameMaster.OnMute += OnMute;
    }

    private void OnDisable()
    {
        GameMaster.OnMute -= OnMute;
    }

    private void OnDestroy()
    {
        GameMaster.OnMute -= OnMute;
    }

    public void PlaySingletonSFX(AudioClip audioClip,float volume,float pitch)
    {
        
        source[1].volume = volume * PlayerPrefs.GetFloat("sfxVolume");
        source[1].pitch = pitch;
        source[1].clip = audioClip;
        source[1].Play();

    }

    public void SetMusicVolume()
    {
        source[0].volume = PlayerPrefs.GetFloat("volume");
    }

    private void Update()
    {
        if (source[1].isPlaying)
        {
            source[0].volume -= Time.deltaTime;
        }
        else if (source[0].volume < PlayerPrefs.GetFloat("volume")) source[0].volume += Time.deltaTime;
    }
}
