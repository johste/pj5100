﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class GameController : MonoBehaviour {

    private int numberOfStepsTaken;
    private int numberOfBlueberries;
    private Text counter;

    public static GameController current;
    public static Dictionary<GameObject,bool> bearsAreFinishedDoingStuff;
 

    public static bool AreBearsFinished()
    {
        foreach(KeyValuePair<GameObject, bool> k in bearsAreFinishedDoingStuff)
        {
            if (!k.Value) return false;
        }
        return true;
    }

    public static void RegisterBear(GameObject bear)
    {
        bearsAreFinishedDoingStuff.Add(bear,true);
    }

    public static void HeyGameImBusy(GameObject bear)
    {
        bearsAreFinishedDoingStuff.Remove(bear);
        bearsAreFinishedDoingStuff.Add(bear,false);
    }

    public static void HeyGameImDoneYo(GameObject bear)
    {
        bearsAreFinishedDoingStuff.Remove(bear);
        bearsAreFinishedDoingStuff.Add(bear, true);
    }

    private void Start()
    {
        current = this;
        counter = Service.Connect("BlueberryCounter").GetComponentInChildren<Text>();
        counter.text = "" + numberOfBlueberries;
    }

	public void AddBlueBerry()
    {
        numberOfBlueberries++;
        counter.text = "" + numberOfBlueberries;
    }

    public void AddBlueBerry(int amount)
    {
        numberOfBlueberries += amount;
        counter.text = "" + numberOfBlueberries;
    }

    private void OnTimeTick()
    {
        numberOfStepsTaken++;
        //Debug.Log(numberOfStepsTaken);
    }

    private void OnEnable()
    {
        GameMaster.OnTimeTick += OnTimeTick;
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    private void OnDestroy()
    {
        Unsubscribe();
    }

    private void Unsubscribe()
    {
        GameMaster.OnTimeTick -= OnTimeTick;
    }

}
