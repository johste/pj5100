﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SardonicMe.Perlib;

public class Service : MonoBehaviour {

    private static List<GameObject> searchables;
    public static SFXManager sfxManager;
    public static Perlib save;
    public static ScreenFader blinds;
    

    void Awake()
    {
        CreateSaveGame();

        searchables = new List<GameObject>();
        sfxManager = GameObject.Find("SFXManager").GetComponent<SFXManager>();
        blinds = GameObject.Find("Blinds").GetComponent<ScreenFader>();
    }

    public static void CreateSaveGame()
    {
        //if (save == null)
        save = new Perlib("save.sav", null);

        save.Open();

        if (!save.HasKey("LevelsUnlocked"))
        {
            save.SetValue("LevelsUnlocked", 0);
            save.SetValue("World", 0);
            Debug.Log("LevelsUnlocked not found.");
            save.Save();
        }
    }

    private void Start()
    {
        Keychain.hasBlueKey = false;
        Keychain.hasGreenKey = false;
        Keychain.hasRedKey = false;
    }

    public static void Register(GameObject searchable)
    {
        searchables.Add(searchable);
    }

    public static GameObject Connect(string gameObjectName)
    {
        return searchables.Find(x => x.name.Contains(gameObjectName));
    }

    public class Keychain
    {
        public static bool hasRedKey;
        public static bool hasBlueKey;
        public static bool hasGreenKey;
    }
}
