﻿using System;
using UnityEngine;

public class GameMaster : MonoBehaviour {

    public bool reloadWhenSeen = true;

    public delegate void TimeTick();
    public static event TimeTick OnTimeTick;
    public static event TimeTick OnLateTimeTick;

    public delegate void MuteEvent();
    public static event MuteEvent OnMute;

    private GameObject tranqImage;
    public static bool tranqSelected;
    public static int tranquilizersLeft = 3;
    public static bool canMove = true;
    public int tranquilizers;

    private bool canTrigger = true;

    public static GameMaster current;
    private int targetLevel;

    private void Awake()
    {
        current = this;
    }

    public void WasSeen()
    {
        if (!reloadWhenSeen) return;
        Service.blinds.CloseBlinds();
        Service.Connect("Canvas").GetComponent<FadeOutWithAlpha>().OnClick();
        ReloadScene(1.5f);  
    }

    public void TriggerOnTimeTick()
    {
        if (OnTimeTick != null && canTrigger) {
            OnTimeTick();
            tranqSelected = false;
            tranqImage.SetActive(tranqSelected);
            canTrigger = false;
        }
    }

    public void TriggerOnLateTimeTick()
    {
        AstarPath.active.Scan();
        if (OnLateTimeTick != null && !canTrigger)
        {
            OnLateTimeTick();
            canTrigger = true;
        }
    }

    private void Start()
    {
        canMove = true;
        tranqImage = Service.Connect("Tranq");
        tranquilizersLeft = tranquilizers;
    }

    public void Mute (bool b)
    {
        PlayerPrefs.SetInt("IsMute", b ? 1 : 0);
        if (OnMute != null)
            OnMute();
	}
	
	public void ReloadScene ()
    {
        Application.LoadLevel(Application.loadedLevel);
	}

    public void ReloadScene(float delay)
    {
        canMove = false;
        Invoke("ReloadScene", delay);
    }
    
    public void ContinueToNextLevel()
    {
        if (Application.loadedLevel + 1 < Application.levelCount)
            Application.LoadLevel(Application.loadedLevel + 1);
        else
            ReloadScene();
    }

    public void ContinueToNextLevel(float delay)
    {
        canMove = false;
        Invoke("ContinueToNextLevel", delay);
    }

    private void GoToLevelInstant()
    {
        if (targetLevel >= Application.levelCount) ReloadScene();
        else
            Application.LoadLevel(targetLevel);
    }

    public void GoToLevel(int targetLevel)
    {
        canMove = false;
        this.targetLevel = targetLevel;
        Invoke("GoToLevelInstant", 1.5f);
    }

    public void ExitLevel()
    {
        if (Application.loadedLevel == 0)
            Application.Quit();
        else
            Application.LoadLevel(0);
    }

    public void ExitLevel(float delay)
    {
        Invoke("ExitLevel", delay);
    }

    public void StartGame()
    {
        int currentLevel = 1 + Service.save.GetValue<int>("World");
        Application.LoadLevel(currentLevel);
    }

    public void StartGame(float delay)
    {
        canMove = false;
        Invoke("StartGame", delay);
    }

    public void EnableTranq(bool b)
    {
        tranqSelected = b && tranquilizersLeft > 0;
        tranqImage.SetActive(tranqSelected);
    }

    public void DeleteSaveGame()
    {
        Service.save.Delete();
        Service.CreateSaveGame();
    }

    void Update()
    {


        if (Application.loadedLevel == 0)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                ExitLevel();
        }
        else if(Application.loadedLevel == 1 || Application.loadedLevel == 2 || Application.loadedLevel == 3 || Application.loadedLevel == 4)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                GoToLevel(0);
                Service.blinds.CloseBlinds();
            }  
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                //CUrrent world.
                GoToLevel(0);
                Service.blinds.CloseBlinds();
            }
        }
    }

}
