﻿using UnityEngine;
using System.Collections;

public class PlaneTextureTiling : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(1.5f * transform.localScale.x,1.5f * transform.localScale.z));
	}

}
