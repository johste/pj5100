For example usage please see Example/SimpleExampleUsage.cs


---Changelog:
1.0.0 - Initial release
1.0.1 - Cleanup.
1.0.2 - Several changes including cleanup, new interface, and namespace changes. This update is not backwards compatible, but updating your code should be trivial.
1.0.3 - Fixed a bug where directory separators were not platform agnostic. (Thanks to Gregory Meach)


---Known issues:
None so far.


For bug reports, feature requests, or any other inquiries please visit our thread:
http://forum.unity3d.com/threads/released-perlib-a-better-file-based-playerprefs.333989/
Or contact me at kaya@sardonic.me