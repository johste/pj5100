﻿using UnityEngine;
using System;
using UnityEditor;
using Pathfinding;

public class ChangeMapSize : EditorWindow {

    int width = 10;
    int height = 10;

    int planeWidth = 10;
    int planeHeight = 10;

    private GameObject plane;
    private GameObject AStarObj;
    private GridGraph gG;
    private VectorGrid vG;
    private bool isOdd;

    [MenuItem("Window/ChangeMapSize")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(ChangeMapSize));
    }


    void OnUpdate()
    {
        plane = GameObject.Find("Plane");
            //AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Forest/Plane.prefab", (typeof(GameObject))) as GameObject;
        AstarPath aPath = GameObject.Find("A*").GetComponent<AstarPath>();
        gG = aPath.astarData.FindGraphOfType(typeof(GridGraph)) as GridGraph;
        vG = GameObject.Find("Grid").GetComponent<VectorGrid>();

        if (width % 2 == 1 || height % 2 == 1)
        {
            isOdd = true;
            return;
        }
        else isOdd = false;

        gG.Width = width;
        gG.Depth = height;
        gG.UpdateSizeFromWidthDepth();
        gG.center = new Vector3(1f, 0f, 1f);
        gG.active.Scan();

        vG.width = width;
        vG.height = height;
        plane.transform.localScale = new Vector3(planeWidth * 0.2f, 1f, planeHeight * 0.2f);
    }

    GUISkin gSkin;
    void OnGUI ()
    {
        if (isOdd)
        {
            var window = EditorWindow.GetWindow(typeof(ChangeMapSize));
            GUI.DrawTexture(new Rect(0, 0, window.position.width, window.position.height), AssetDatabase.LoadAssetAtPath("Assets/ThisIsFine.jpg", (typeof(Texture))) as Texture, ScaleMode.StretchToFill);
            GUILayout.Label("Must be an even number!", EditorStyles.helpBox);
        }

        GUILayout.Label("Map Size", EditorStyles.boldLabel);
        width = EditorGUILayout.IntField("Grid Width", width);
        height = EditorGUILayout.IntField("Grid Height", height);
        planeWidth = EditorGUILayout.IntField("Plane Width", planeWidth);
        planeHeight = EditorGUILayout.IntField("Plane Height", planeHeight);

        //SetPlaneSize plane = (SetPlaneSize)target;
        if (GUILayout.Button("Update"))
        {
            try
            {
                OnUpdate();
            }
            catch (Exception e)
            {
                Debug.LogError("Something went wrong. Click EngineSystems -> A* object in Hierachy and Try again.");
            }
           
        }
        
    }
}
