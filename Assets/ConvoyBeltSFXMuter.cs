﻿using UnityEngine;
using System.Collections;

public class ConvoyBeltSFXMuter : MonoBehaviour {

    AudioSource _this;

	void Start () {
        _this = GetComponent<AudioSource>();
        OnMute();
    }

    private void OnMute()
    {
        if (PlayerPrefs.GetInt("IsMute") == 1) Pause();
        else
            Continue();

    }

    private void Pause()
    {
        _this.Pause();
    }

    private void Continue()
    {
        _this.Play();
    }

    private void OnEnable()
    {
        GameMaster.OnMute += OnMute;
    }

    private void OnDisable()
    {
        GameMaster.OnMute -= OnMute;
    }

    private void OnDestroy()
    {
        GameMaster.OnMute -= OnMute;
    }
}
